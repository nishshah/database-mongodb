const mongoose = require('mongoose')


 const purchase_orders= new mongoose.Schema({
     product:{type:String, trim:true},
     total:{type:Number, trim:true},
     ref:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
     }
 })  

module.exports=mongoose.model('purchase_orders',purchase_orders)