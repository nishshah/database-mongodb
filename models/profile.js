const { process_params } = require('express/lib/router')
const mongoose = require('mongoose')
const mongoose_paginate = require('mongoose-paginate')

const Profile = new mongoose.Schema({
    firstName:{type: String,trim:true},
    lastName:{type: String,trim:true},
    age:{type: Number},
    mail:{type: String}
},
{ time:{timestamps: true} })

Profile.plugin(mongoose_paginate)

Profile.virtual("order", {
	ref: "order",
	localField: "_id",
	foreignField: "firstName",
})
Profile.pre("save", (next) =>{
	let now = Date.now()
	console.log("pre save")
	this.updatedAt = now
	if (!this.createdAt) {
		this.createdAt = now;
	}
	next()
})

module.exports=mongoose.model('Profile',Profile)