const express = require('express');
const { route } = require('express/lib/router');
const router = express.Router()
const mongoose = require("mongoose");
const profile = require('../models/profile')
const purchase_orders = require('../models/order')

router.post('/signup',(req,res)=>{
    const person = new profile({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        age: req.body.age,
        mail: req.body.mail
    })
    person.save()
    .then(result=>{
        console.log(result)
        res.status(200).json({person})
    })
    
})
router.get('/all',(req,res)=>{
    const { page , perpage }= req.query
    const options={
        page : parseInt(page , 10) || 1,
        limit: parseInt(perpage, 10) || 10
    }
    const all = profile.paginate({},options).then(data=>{
        res.status(200).json(data)
    })
})

router.get('/id',(req,res)=>{
   
    profile.find().select().exec().then(doc=>{
        console.log(doc)
        const response= {
            count: doc.length,
            profile: doc.map(docs =>{
                return{
                    firstName: docs.firstName,
                    lastName: docs.lastName,
                    age: docs.age,
                    mail:docs.mail,
                    _id: docs._id
                }
            })
        }
        res.status(200).json(response)
    })
})
router.get('/aggregate',(req,res)=>{
    const options = {
            $project:{"Name":"$firstName",_id:0,age:1,mail:1}
            // $group:{_id:"Random",allAges:{$sum:"$age"},minAge:{$min:"$age"},maxAge:{$max:"$age"},avgAge:{$avg:"$age"}}
        }
   
    profile.aggregate([options,{$sort:{age:-1}}]).then(doc=>{
        console.log(doc)
        const response= {
            profile: doc.map(docs =>{
                return{
                    // docs
                    Name: docs.Name,
                    age: docs.age,
                    mail:docs.mail,
                }
            })
        }
        res.status(200).json(response)
    })
})
router.get('/id/update',(req,res)=>{
    const query=req.query
    const id = query.id
    delete query['id']
    profile.findByIdAndUpdate(id, query ,{new:true}).select().then(profile=>{
        
        res.status(200).json(profile)
    })
})

router.get('/id/remove',(req,res)=>{
    const query=req.query
    profile.findByIdAndRemove(query.id).then(data=>{
        
        res.status(200).json({
            message:"deleted",
            check:"localhost:3000/id"
        })
    })
})

router.get('/one',(req,res)=>{
    const regex = new RegExp(escapeRegex(req.query.name), 'gi')
    console.log(regex)
    profile.find({ firstName: regex}).select().exec().then(docs=>{
        const response= {
            count: docs.length,
            profile: docs.map(doc =>{
                return{
                    firstName: doc.firstName,
                    lastName: doc.lastName,
                    age: doc.age,
                    mail:doc.mail,
                    _id: doc._id
                }
            })
        }
        res.status(200).json(response)
    }).catch(err=>{
        res.status(500).send(err)
    })
})
router.get('/id/:byID',(req,res)=>{
    const id=req.params.byID
    profile.findById(id).select().exec().then(doc=>{
        const response= {
            count: doc.length,
            profile: {
                    firstName: doc.firstName,
                    lastName: doc.lastName,
                    age: doc.age,
                    mail:doc.mail,
                    _id:doc._id
                }

        }
        res.status(200).json(response)
    })
})
router.post('/order/:id',(req,res)=>{
    const id = req.params.id
    const orders = new purchase_orders({
        product: req.body.product,
        total: req.body.total,
        ref: id
    })
    orders.save()
    .then(result=>{
        console.log(result)
        res.status(200).json({result})
    })
    
})

const escapeRegex=(text)=> {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

module.exports = router;