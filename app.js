const express = require('express')
const mongose = require('mongoose')
const bodyparser = require('body-parser')
const home = require('./routes/home')

const app = express()
mongose.connect('mongodb://localhost/ExData',{
    useNewUrlParser: true })   
    .then(() => console.log("Database connected!"))
    .catch(err => console.log(err))

app.use(bodyparser.urlencoded({extended: false}))
app.use(bodyparser.json())
app.use('/',home)
app.listen(3000,()=>{
    console.log("Sever is On port: 3000!")
}) 